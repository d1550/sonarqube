# Пример использования
## Visual studio
```bash
dotnet sonarscanner begin /k:"Test" /d:sonar.host.url=$SONAR_URL  /d:sonar.login=$SONAR_LOGIN
dotnet build -c Release
dotnet sonarscanner end /d:sonar.login=$SONAR_LOGIN
```
